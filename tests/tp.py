def nombre_entier(n: int) -> str:
    if n == 0: return '0'
    return 'S' + nombre_entier(n - 1)


def S(n: str) -> str:
    return 'S' + n
    

def addition(a: str, b: str) -> str:
    if a == "0": return b
    if a.startswith("S"):
        return S(addition(a[1:], b))


def multiplication(a: str, b: str) -> str:
    if a == "0": return 0
    return addition(b[1:], multiplication(a[1:], b))


def facto_ite(n: int) -> int:
    res = 1
    while n > 0:
        res = res * n
        n = n - 1
    return res


def facto_rec(n: int) -> int:
    
    cache = {}

    def f(x: int) -> int:
        if x in cache:
            return cache[x]
        elif x == 0:
            result = 1
        else:
            result = x * f(x-1)
        cache[x] = result
        return result
    return f(n)



def fibo_rec(n: int) -> int:
    cache = {}

    def f(x: int):
        if x in cache:
            return cache[x]
        elif x == 0:
            result = 0
        elif x == 1:
            result = 1
        else:
            result = f(x-1) + f(x-2)
        cache[x] = result
        return result

    return f(n)


def fibo_ite(n: int) -> int:
    if n < 0:
        raise ValueError("n doit être positif ou nul")
    elif n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        fib_pp = 0
        fib_p = 1
        for i in range(2, n+1):
            fib = fib_p + fib_pp
            fib_pp = fib_p
            fib_p = fib
        return fib


def golden_phi(n: int) -> int:
    pass


def sqrt5(n: int) -> int:
    pass


def pow(a: float, n: int) -> float:
    pass