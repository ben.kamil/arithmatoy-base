#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

#define MAX_LEN 4096

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

int get_max(unsigned int a, unsigned int b) { return a >= b ? a : b; }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  char *tmp = (char *)malloc(MAX_LEN * sizeof(char));
  if (NULL == tmp) {
    fprintf(stderr, "[-]: Malloc\n");
    return NULL;
  }

  unsigned int index, lhs_value, rhs_value, sum;
  unsigned char carry;
  size_t len_lhs, len_rhs;
  int max_len, lhs_index, rhs_index;

  len_lhs = strlen(lhs);
  len_rhs = strlen(rhs);
  // la plus grande longeur entre lhs et rhs
  max_len = get_max(len_lhs, len_rhs);
  // indice de la chaine result
  index = 0;
  // variable qui va stocker la retenu
  carry = 0;
  // index des chaines lhs et rhs
  lhs_index = len_lhs - 1;
  rhs_index = len_rhs - 1;
  // valeurs qui seront lue
  lhs_value = 0;
  rhs_value = 0;

  while (max_len >= 0) {
    lhs_value = lhs_index >= 0 ? get_digit_value(lhs[lhs_index--]) : 0;
    rhs_value = rhs_index >= 0 ? get_digit_value(rhs[rhs_index--]) : 0;

    sum = lhs_value + rhs_value + carry;
    if (sum >= base) {
      sum -= base;
      carry = 1;
    } else {
      carry = 0;
    }
    // result[index++] = to_digit(sum);
    tmp[index++] = to_digit(sum);
    max_len -= 1;
  }

  if (carry) {
    tmp[index++] = to_digit(carry);
  }

  tmp[index] = '\0';
  tmp = reverse(tmp);

  /**
   * On enlève les 0 qui sont devant
   * la fonction drop_leading_zeros renvoie
   * le pointeur après enlevement des zeros
   *
   * On compte le nombre de caractere qui reste
   * en utilisant strlen qui à partir du pointeur
   * renvoyé par drop_leading_zeros nous donne
   * la taille correcte
   */
  const char *t = drop_leading_zeros(tmp);

  int len_res = strlen(t);
  char *result = malloc(len_res);
  memcpy(result, t, len_res);
  result[len_res] = '\0';

  free(tmp);
  tmp = NULL;

  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
  return result;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  int lhs_len = strlen(lhs);
  int rhs_len = strlen(rhs);

  // Allocate memory to store the result (maximum length = lhs_len)
  int result_len = lhs_len + 1; // +1 for '\0'
  char *result = (char *)malloc(MAX_LEN * sizeof(char));
  memset(result, '0', MAX_LEN); // Initialize the result with '0'
  // result[MAX_LEN - 1] = '\0'; // Null-terminate the result
  int index = 0;

  int i, j;
  int borrow = 0;
  for (i = lhs_len - 1, j = rhs_len - 1; i >= 0; i--, j--) {
    int subtrahend = (j >= 0) ? (get_digit_value(rhs[j])) : 0;
    int minuend = get_digit_value(lhs[i]) - borrow;

    if (minuend < subtrahend) {
      minuend += base;
      borrow = 1;
    } else {
      borrow = 0;
    }

    int difference = minuend - subtrahend;
    // result[i] = difference + '0';
    result[index++] = to_digit(difference);
  }

  result[index] = '\0';

  // Remove leading zeros from the result
  char *final_result = reverse(result);
  while (*final_result == '0' && *(final_result + 1) != '\0') {
    final_result++;
  }

  // If the result is "0", return a single-character string
  if (*final_result == '\0') {
    final_result--;
  }

  return final_result;
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  int lhs_len = strlen(lhs);
  int rhs_len = strlen(rhs);

  // Allocate memory to store the result (maximum length = lhs_len + rhs_len)
  int result_len = lhs_len + rhs_len + 1; // +1 for '\0'
  char *result = (char *)malloc(result_len * sizeof(char));
  memset(result, '0', result_len); // Initialize the result with '0'
  result[result_len - 1] = '\0'; // Null-terminate the result

  int i, j;
  for (i = lhs_len - 1; i >= 0; i--) {
    int carry = 0;
    int multiplier = get_digit_value(lhs[i]);

    for (j = rhs_len - 1; j >= 0; j--) {
      int multiplicand = get_digit_value(rhs[j]);
      int product = (multiplier * multiplicand) + carry +
          (get_digit_value(result[i + j + 1]));

      carry = product / base;
      result[i + j + 1] = to_digit((product % base));
    }

    if (carry > 0) {
      result[i + j + 1] = to_digit(carry);
    }
  }

  // Remove leading zeros from the result
  char *final_result = (result);
  while (*final_result == '0' && *(final_result + 1) != '\0') {
    final_result++;
  }

  // If the result is "0", return a single-character string
  if (*final_result == '\0') {
    final_result--;
  }

  return final_result;
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
